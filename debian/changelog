episoder (0.7.1-3) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:11:48 -0500

episoder (0.7.1-2) unstable; urgency=medium

  * Team upload.
  * Convert from dh_pysupport to dh_python2                     closes: #786047
  * Bumped Standards-Version to 3.9.6, no changes needed

 -- Piotr Ożarowski <piotr@debian.org>  Wed, 19 Aug 2015 23:40:58 +0200

episoder (0.7.1-1) unstable; urgency=low

  * New upstream release					closes: #740411
  * Updated upstream homepage in debian/control, watch, and copyright
  * Bumped Standards-Version to 3.9.5
  * debian/patches:
    - Refreshed 01_do-not-install-doc-files.patch
    - Refreshed 02_awk-file-path.patch

 -- Stefan Ott <stefan@ott.net>  Sun, 27 Apr 2014 18:35:59 +0200

episoder (0.7.0-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Stefan Ott ]
  * New upstream release
  * Bumped Standards-Version to 3.9.4, no changes needed
  * debian/control:
    - Updated upstream homepage
    - Updated package description
    - Added dependencies for python-argparse and python-tvdb-api
    - Dropped dependency for python-beautifulsoup
  * debian/copyright:
    - Updated upstream homepage
    - Updated copyright line
    - Changed upstream license to GPLv3
  * debian/watch: use new upstream homepage
  * debian/docs:
    - Don't try to install README.tvcom which was dropped from upstream
    - Moved examples to debian/examples
  * debian/patches:
    - Refreshed 01_do-not-install-doc-files.patch
    - Added 02_awk-file-path.patch
    - Added 03_manpage-path.patch

 -- Stefan Ott <stefan@ott.net>  Mon, 14 Oct 2013 02:34:47 +0200

episoder (0.6.5-1) unstable; urgency=low

  * New upstream release
  * Bumped Standards-Version to 3.9.1, no changes needed
  * debian/watch: Removed workaround for google code

 -- Stefan Ott <stefan@ott.net>  Wed, 06 Oct 2010 03:09:01 +0200

episoder (0.6.4-1) unstable; urgency=low

  * Initial official debian package				closes: #322969

 -- Stefan Ott <stefan@ott.net>  Fri, 25 Jun 2010 04:24:36 +0200
